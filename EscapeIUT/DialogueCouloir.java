import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import javax.swing.JFrame;

import java.awt.Dimension;
import java.io.*;

public class DialogueCouloir extends Dialogue{

    public void choisirSalle(int PV){
      p.setPV(PV);
      String choisirSalle = "./textes/dial_couloir/choisir_salle.txt";
      System.out.println(Read.readLineByLine(choisirSalle));
      Scanner sc = new Scanner(System.in);
      int choix = sc.nextInt();
        switch (choix) {
          case 1:
            (new DialogueSallePC()).Intro_Salle_Informatique();
            break;
          case 2:
            (new DialogueBureauProf()).Intro_Bureau_Prof();
            break;
          case 3:
            (new DialogueSalle105()).Intro_Salle105();
            break;
          case 4:
            (new DialogueToilettes()).Intro_Toilettes();
            break;
          case 5:
            (new Inventaire()).Intro_Inventaire();
            break;
          case 6:
            int e = 1;
            int salle = 1;
            JFrame fenetre = new JFrame ("Plan de l'IUT :");
            Plan dessin = new Plan(e,salle);
            dessin.setPreferredSize(new Dimension (1200,480));
            fenetre.setContentPane(dessin);
            fenetre.pack();
            fenetre.setVisible(true);
            choisirSalle(p.getPV());
            break;
          default:
            System.out.println("Choix non reconnu");
            choisirSalle(p.getPV());
            break;

        }
      }

}
