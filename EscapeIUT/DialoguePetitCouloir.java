import java.util.Scanner;
import java.util.Random;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DialoguePetitCouloir extends Dialogue {

    public void Petit_Couloir(boolean b){
      String petit_couloir = "./textes/dial_petitcouloir/dial_petitcouloir.txt";
      System.out.println(Read.readLineByLine(petit_couloir));
      if (b == true){
        Recite_RGPD();
      }else{
        Recite_pas_RGPD();
      }
    }

    public int attaqueTenib(){
        Random random = new Random();
        int degats = random.nextInt(6);
        return degats;
    }

  public void Recite_pas_RGPD() {
    String petit_couloir2 = "./textes/dial_petitcouloir/recite_pas.txt";
    System.out.println(Read.readLineByLine(petit_couloir2));
    int a = attaqueTenib();
    int b = attaqueTenib();
    int degats = a + b;
    p.setPV(p.getPV()-degats);
    System.out.println("Vous avez pris "+degats+" point(s) de degats\n");
    System.out.println("");
    System.out.println("Vous avez  " +p.getPV()+ "/"+ p.getMaxPV()+ "PV\n");
    System.out.println("");
  }

  public void Recite_RGPD(){

  Scanner sc = new Scanner(System.in);
  int choix = sc.nextInt();
    switch (choix) {
      case 1:
        String recite1 = "./textes/dial_petitcouloir/recite_case1.txt";
        System.out.println(Read.readLineByLine(recite1));
        Recite_pas_RGPD();
        break;
      case 2:
        String recite2 = "./textes/dial_petitcouloir/recite_case2.txt";
        System.out.println(Read.readLineByLine(recite2));
        (new DialogueBureauIUT()).Intro_Bureau_IUT();
        break;
      default:
        System.out.println("Choix non reconnu");
        Recite_RGPD();
        break;
      }
  }


}
