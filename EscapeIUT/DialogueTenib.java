package com.iut.escapeiut;
import java.util.Scanner;
import java.util.Random;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DialogueTenib extends Dialogue {

    public void AppelTenib(int PV) {
        p.setPv(PV);
        System.out.println(p.getPv());
        Scanner sc = new Scanner(System.in);
        String filePath = "./textes/dial_tenib/appeltenib.txt";
        System.out.println(Read.readLineByLine(filePath));
        //System.out.println("TENIB : "+p.setNom()+"\n");
        System.out.print("Choisissez votre nom : ");
        p.setNom(sc.nextLine());
        System.out.println("");
        Choix_AppelTenib(p.getPv());
    }

    public int Choix_AppelTenib(int PV) {
        p.setPv(PV);
        Choix_AppelTenib(p.getPV());
    }

    public int Choix_AppelTenib(int PV) {
        p.setPV(PV);
        String filePath2 = "./textes/dial_tenib/appeltenib2.txt";
        System.out.println(Read.readLineByLine(filePath2));
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();

        switch (choix) {
            case 1:
            String filePath3 = "./textes/dial_tenib/tenibappel_choix1.txt";
            System.out.println(Read.readLineByLine(filePath3));
            ReciteRGPD(p.getPv());
            break;
            case 2:
            String filePath4 = "./textes/dial_tenib/tenibappel_choix2.txt";
            System.out.println(Read.readLineByLine(filePath4));
            ReciteRGPD(p.getPv());
            break;
            case 3:
            String filePath5 = "./textes/dial_tenib/tenibappel_choix3.txt";
            System.out.println(Read.readLineByLine(filePath5));
            ReciteRGPD(p.getPv());
            break;
            case 4:
            String filePath6 = "./textes/dial_tenib/tenibappel_choix4.txt";
            System.out.println(Read.readLineByLine(filePath6));
            baisserPV();

            if (p.getPV() <= 0) {
                Mort.GameOver();
            }
            default:
            System.out.println("Choix non reconnu");
            Choix_AppelTenib(p.getPv());
            break;
        }
        return PV;
    }

    public void ReciteRGPD(int PV) {
        p.setPV(PV);
        String filePath7 = "./textes/dial_tenib/reciteRGPD.txt";
        System.out.println(Read.readLineByLine(filePath7));
        ProfTableau(p.getPv());
    }

    public void ProfTableau(int PV) {
        p.setPv(PV);
        ProfTableau(p.getPV());
    }

    public void ProfTableau(int PV) {
        p.setPV(PV);
        String filePath8 = "./textes/dial_tenib/proftableau.txt";
        System.out.println(Read.readLineByLine(filePath8));
        Choix_ProfTableau(p.getPV());
      }

    public int Choix_ProfTableau(int PV) {
        p.setPV(PV);
        String filePath9 = "./textes/dial_tenib/proftableau_suite.txt";
        System.out.println(Read.readLineByLine(filePath9));
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            System.out.println("Vous attendez");
            for (int i = 0; i < 100; i++) {
                System.out.println("...");
            }
            System.out.println("C'est long n'est-ce pas ?");
            for (int i = 0; i < 100; i++) {
                System.out.println("...");
            }
            System.out.println("Il serait plus interessant de faire autre chose");
            Choix_ProfTableau(p.getPV());
            break;
            case 2:
            String filePath10 = "./textes/dial_tenib/proftableau_case2.txt";
            System.out.println(Read.readLineByLine(filePath10));
            Scanner sc1 = new Scanner(System.in);
            Choix_ProfTableau(p.getPv());
            break;
            case 3:
            String filePath11 = "./textes/dial_tenib/proftableau_case3.txt";
            System.out.println(Read.readLineByLine(filePath11));
            Scanner sc2 = new Scanner(System.in);
            TentativeFuite_TENIB(p.getPV());
            break;
            case 4:
            String filePath12 = "./textes/dial_tenib/proftableau_case4.txt";
            System.out.println(Read.readLineByLine(filePath12));
            Scanner sc3 = new Scanner(System.in);
            baisserPV();
            if (p.getPV() <= 0)
            {
              Mort.GameOver();
              break;
            } else {
                Choix_ProfTableau(p.getPV());
              break;
            }
            default:
            System.out.println("Choix non reconnu");
            Choix_ProfTableau(p.getPv());
            break;
        }
        return PV;
    }

    public int TentativeFuite_TENIB(int PV) {
        p.setPV(PV);
        String TentativeFuite = "./textes/dial_tenib/fuite_tenib.txt";
        System.out.println(Read.readLineByLine(TentativeFuite));
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            String TentativeFuite1 = "./textes/dial_tenib/fuite_tenib_case1.txt";
            System.out.println(Read.readLineByLine(TentativeFuite1));
            baisserPV();
            if (p.getPV() <= 0) {
                Mort.GameOver();
                break;
            } else {
                Choix_ProfTableau(p.getPv());
                break;
            }
            case 2:
            String TentativeFuite2 = "./textes/dial_tenib/fuite_tenib_case1.txt";
            System.out.println(Read.readLineByLine(TentativeFuite2));
            baisserPV();
            if (p.getPV() <= 0) {
                Mort.GameOver();
                break;
            } else {
                Choix_ProfTableau(p.getPv());
                break;
            }
            case 3:
            String TentativeFuite3 = "./textes/dial_tenib/fuite_tenib_case3.txt";
            System.out.println(Read.readLineByLine(TentativeFuite3));
            Choix_ProfTableau(p.getPv());
            break;
            case 4:
            String TentativeFuite4 = "./textes/dial_tenib/fuite_tenib_case4.txt";
            System.out.println(Read.readLineByLine(TentativeFuite4));
            (new DialogueCouloir()).choisirSalle(p.getPv());
            break;
            default:
            System.out.println("Choix non reconnu");
            TentativeFuite_TENIB(p.getPv());
            (new DialogueCouloir()).choisirSalle(p.getPV());
            break;
        }
        return p.getPv();
    }

    public void baisserPV(){
      Random random = new Random();
      int nb = random.nextInt(6);
      p.setPv(p.getPv()-nb);
      System.out.println("Vous avez pris "+nb+" point(s) de degats, il vous reste " + p.getPv() + "/" + p.getMaxPV() + " PV.");
    }
}
