import java.util.Scanner;
import java.util.Random;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FuiteFinale extends Fin
//public class FuiteFinale
{
    private int pasPerso, pasMonstre;
    private static int pas=0;
    private static int monstre=0;
    private static int compteurPas=5;
    private static int compteurMonstre=0;
    private static int compteurFin = 50;
    private static int total = 0;

  //  public FuiteFinale(){}

    public int avancer(){
        Random random = new Random();
        int nb = random. nextInt(6);
        return nb;
    }

    public int reculer(){
        Random random = new Random();
        int nb = random. nextInt(6);
        return nb;
    }

    public void commencerFuiteFinale(){
        dialogueFuiteFinale();
    }

    public void dialogueFuiteFinale(){
        String fuite = "./textes/fuite_finale/dial_fuite_finale.txt";
        System.out.println(Read.readLineByLine(fuite));
        choisirCourirOuRouleau();
    }

    public void choisirCourirOuRouleau(){
        Scanner sc=new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            courir();
            break;
            case 2:
            lacherRouleau();
            break;
        }
    }

    public void dialogueBougerVous(){
        System.out.println("Vous avez bouge de " +pas+ " pas\n");
        System.out.println("Vous avez realise en tout " +compteurPas+ "/"+ compteurFin+ " pas\n");
    }

    public void dialogueBougerMonstre(){
        System.out.println("Votre adversaire a bouge de " +monstre+ " pas\n");
        System.out.println("Elle est a " +compteurMonstre+ "/"+ compteurFin+"\n");
    }

    public void bouger(){
        bougerVous();
        bougerMonstre();
    }

    public void bougerVous(){
        pas = avancer();
        compteurPas+=pas;
        total = compteurPas - compteurMonstre;
        dialogueBougerVous();
    }

    public void bougerMonstre(){
        monstre = avancer();
        compteurMonstre+=monstre;
        total = compteurPas - compteurMonstre;
        dialogueBougerMonstre();
    }

    public void reculerMonstre(){
      monstre = -(reculer());
      compteurMonstre+=monstre;
      total = compteurPas - compteurMonstre;
      dialogueBougerMonstre();
    }

    public void courir(){
        bouger();
        if(attraper()){
            declencherSequenceEchec();
        }
        else if(gagner()){
            System.out.println("Vous inserez la cle dans la serrure.\n");
            bougerMonstre();
            if(attraper())
                declencherSequenceEchec();
            else{
                System.out.println("Vous deverrouillez la porte.\n");
                bougerMonstre();
                if(attraper())
                    declencherSequenceEchec();
                else{
                    System.out.println("Vous poussez la porte de l'entree principale et vous sortez de l'IUT, vous avez tellement peur que vous oubliez les escaliers et vous tombez. \n");
                    p.setPV(p.getPV()-2);
                    System.out.println("Vous subissez 2 point(s) de dégâts. Il vous reste " +p.getPV()+ " PV. \n");
                    if(p.getPV()==0)
                        (new Mort()).mourirTomber();
                    else
                        declencherSequenceReussite();
                }
            }
        }else{
            dialogueFuiteFinale();
        }
    }

    public void lacherRouleau(){
        reculerMonstre();
        dialogueFuiteFinale();
    }

    public boolean attraper(){
        return (total<=0);
    }

    public boolean gagner(){
        return (compteurPas>=50);
    }

    public void declencherSequenceEchec(){
      String echec = "./textes/fuite_finale/sequence_echec.txt";
      System.out.println(Read.readLineByLine(echec));
    }

    public void declencherSequenceReussite(){
      String reussite = "./textes/fuite_finale/sequence_reussite.txt";
      System.out.println(Read.readLineByLine(reussite));

    }

    /*public static void main (String[]args){
      FuiteFinale f = new FuiteFinale();
      f.commencerFuiteFinale();
    } */

}
