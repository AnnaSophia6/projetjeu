package com.iut.escapeiut;
/**
 *
 */

/**
 *
 *
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class Plan extends JPanel {
  private int salle;
  private int epaisseur;
  public final static Color maroon= new Color (140,106,60);
  public final static Color yellowdark= new Color (200,200,0);
  public final static Color lightblue = new Color(0,255,255);
  public final static Color purple = new Color(114,36,175);
  public final static Color grey = new Color (175,175,175);
  public final static Color lightgreen = new Color(136,215,177);

  public Plan(int e, int sa){
    this.epaisseur = e;
    this.salle = sa;

  }

  public void paintComponent (Graphics g){
    super.paintComponent(g);
    g.setColor(Color.black);
    g.drawRect(50,150,199,100);
    String s = "Cafeteriat";
    g.drawString(s, 75,175);
    g.drawRect(50,250,199,100);
    s = "Hall IUT";
    g.drawString(s, 75,275);

    g.setColor(Color.green);
    g.drawRect(50,75,200,74);
    s = "Espace Fumeur";
    g.drawString(s, 115,110);

    g.setColor(Color.blue);
    g.drawRect(250,150,100,74);
    s = "Salle 101";
    g.drawString(s, 275,175);

    g.drawRect(350,150,100,74);
    s = "Salle 103";
    g.drawString(s, 375,175);

    g.drawRect(450,150,100,74);
    s = "Salle 105";
    g.drawString(s, 475,175);

    g.drawRect(550,150,99,74);
    s = "Salle 107";
    g.drawString(s, 575,175);

    g.setColor(yellowdark);
    g.drawRect(650,150,99,74);
    s = "Salle PC";
    g.drawString(s, 675,175);

    g.setColor(Color.magenta);
    g.drawRect(250,225,499,49);
    s = "Couloir";
    g.drawString(s, 480,250);

    g.setColor(purple);
    g.drawRect(250,275,84,75);
    s = "Secretariat";
    g.drawString(s, 250,310);

    g.setColor(Color.red);
    g.drawRect(335,275,85,75);
    s = "Bureau";
    g.drawString(s, 345,290);
    s = "EGAIRAM";
    g.drawString(s, 345,320);


    g.drawRect(420,275,85,75);
    s = "Bureau";
    g.drawString(s, 430,290);
    s = "LEGEOR";
    g.drawString(s, 430,320);

    g.drawRect(505,275,85,75);
    s = "Bureau";
    g.drawString(s, 515,290);
    s = "TENIB";
    g.drawString(s, 515,320);

    g.drawRect(590,275,85,75);
    s = "Bureau";
    g.drawString(s, 600,290);
    s = "TENODAL";
    g.drawString(s, 600,320);

    g.setColor(maroon);
    g.drawRect(675,275,74,75);
    s = "Toilettes";
    g.drawString(s, 680,310);

    g.setColor(lightblue);
    g.drawRect(750,150,199,200);
    s = "Bibliotheque";
    g.drawString(s, 825,250);

    g.setColor(grey);
    g.drawRect(950,225,99,50);
    s = "Petit couloir";
    g.drawString(s, 960,250);

    g.setColor(lightgreen);
    g.drawRect(1050,175,100,150);
    s = "Bureau";
    g.drawString(s, 1070,200);
    s = "DIRECTEUR";
    g.drawString(s, 1070,230);
    s = "IUT";
    g.drawString(s, 1070,260);


    if (salle == 0){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 100,250);
    }

    if (salle == 1){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 265,250);
    }

    if (salle == 2){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 200,250);
    }

    if (salle == 3){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 150,110);
    }

    if (salle == 4){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 300,190);
    }

    if (salle == 5){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 400,250);
    }

    if (salle == 6){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 500,250);
    }

    if (salle == 7){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 600,250);
    }

    if (salle == 8){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 700,250);
    }

    if (salle == 9){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 320,280);
    }

    if (salle == 10){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 405,280);
    }

    if (salle == 11){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 490,280);
    }

    if (salle == 12){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 575,280);
    }

    if (salle == 13){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 660,280);
    }

    if (salle == 14){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 735,280);
    }

    if (salle == 15){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 850,230);
    }

    if (salle == 16){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 1000,280);
    }

    if (salle == 17){
      g.setColor(Color.black);
      s = "X";
      g.drawString(s, 1100,170);
    }



  }

  public void setEpaisseur(int e){
    this.epaisseur = e;
    repaint();
  }

}
