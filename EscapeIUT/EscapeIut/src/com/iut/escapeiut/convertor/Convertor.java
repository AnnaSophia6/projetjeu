package com.iut.escapeiut.convertor;
import java.io.File;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;


import com.iut.escapeiut.xml.Perso;



public class Convertor {
	
	public static final String SAVEFILE = "sauve.xml";
	
	// QUAND ON VEUT CHARGER LA SAUVEGARDE, CETTE METHODE SERA APPELLÉE, quand on commence le jeu, il transforme
	// le doc xml en java
	private static Perso chargeSaveFile() throws JAXBException, IOException {
        JAXBContext context = JAXBContext.newInstance(Perso.class);
        return (Perso) context.createUnmarshaller()
                .unmarshal(new FileReader(SAVEFILE));
    }
	
	// il va transformer le java en xml et le souvegarder en forme de fichier
	public static void saveState(Perso perso) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance("com.iut.escapeiut.xml");
        Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(perso,new File(SAVEFILE));
    }
	
	public static Perso demanderChargerSauvegarde() throws JAXBException, IOException {
		

		Scanner sc = new Scanner(System.in);
		File file = new File(SAVEFILE);
        if(file.canRead()){
            System.out.println("Charger la sauvegarde ? O/N");
            if(sc.nextLine().toUpperCase().equals("O")){
                return chargeSaveFile();
                
          
                
               
            }
        }
      return new Perso("inconnu",25);
    }
	

}
