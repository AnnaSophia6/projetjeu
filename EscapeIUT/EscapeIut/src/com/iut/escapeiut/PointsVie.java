package com.iut.escapeiut;

/**
 * @author asjos
 */
public interface PointsVie
{
    public static final int PV=25;
    public static final int FACILE=PV;
    public static final int MOY=PV-10;
    public static final int DIFF=PV-15;
    public static final int DIABO=PV-20;

}
