package com.iut.escapeiut;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import com.iut.escapeiut.convertor.Convertor;
import com.iut.escapeiut.xml.Perso;

/**
 * Classe Game est un jeu en particulier avec un personnage
 *
 * @author bernar276u, asjos
 *
 */
public class Game {
    public Perso p;

    public Game(){
        this.p = new Perso("inconnu",25);
    }

    public void startGame(){
        Demarrez de = new Demarrez() ;
        de.lancement();
        //p.choisirNom();

        (new DialogueJeu()).intro();
    }

    public static void main(String[] args) throws JAXBException, IOException {
    	
     Perso p = Convertor.demanderChargerSauvegarde();
     
      Convertor.saveState(p);
        Demarrez de = new Demarrez();
        de.lancement();
        (new DialogueJeu()).intro();
        System.out.println("Au revoir");
    }
 }
