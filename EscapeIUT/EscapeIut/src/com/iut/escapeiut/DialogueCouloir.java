package com.iut.escapeiut;
import java.util.Scanner;

import javax.swing.JFrame;

import java.awt.Dimension;
import java.io.*;

import com.iut.escapeiut.xml.Perso;

public class DialogueCouloir extends Dialogue{



    public void choisirSalle(int PV){
      p.setPv(PV);
      System.out.println("Vous etes maintenant seul dans ce couloir.");
      System.out.println("");
      System.out.println("Que faites-vous ?");
      System.out.println("");
      System.out.println("   1)   Je vais dans la salle informatique");
      System.out.println("   2)   Je vais vers le bureau de TENIB");
      System.out.println("   3)   Je vais vers la salle 105");
      System.out.println("   4)   Je vais aux toilettes");
      System.out.println("   5)   Je verifie mon inventaire");
      System.out.println("   6)   Afficher le plan de l'IUT");
      System.out.println("");
      Scanner sc = new Scanner(System.in);
      int choix = sc.nextInt();
        switch (choix) {
          case 1:
            (new DialogueSallePC()).Intro_Salle_Informatique();
            break;
          case 2:
            (new DialogueBureauProf()).Intro_Bureau_Prof();
            break;
          case 3:
            (new DialogueSalle105()).Intro_Salle105();
            break;
          case 4:
            (new DialogueToilettes()).Intro_Toilettes();
            break;
          case 5:
            (new Inventaire()).Intro_Inventaire();
            break;
          case 6:
            int e = 1;
            int salle = 1;
            JFrame fenetre = new JFrame ("Plan de l'IUT :");
            Plan dessin = new Plan(e,salle);
            dessin.setPreferredSize(new Dimension (1200,480));
            fenetre.setContentPane(dessin);
            fenetre.pack();
            fenetre.setVisible(true);
            choisirSalle(p.getPv());
            break;
          default:
            System.out.println("Choix non reconnu");
            choisirSalle(p.getPv());
            break;

        }
      }

}
