import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;
=======
package com.iut.escapeiut;

/**
 * @author asjos, ek
 */
public class Mort extends Dialogue
{

  public static void GameOver()
  {
    String gameOver = "./textes/game_over.txt";
    System.out.println(Read.readLineByLine(gameOver));
  }

  public void mourirTomber()
  {
      String dead = "./textes/mort.txt";
      System.out.println(Read.readLineByLine(dead));
      GameOver();
  }
}
