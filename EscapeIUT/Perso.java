/**
 * @author asjos
 */

public class Perso
{
    private String nom;
    private int pv;
    private int MaxPV;

    /**
     * @param nom
     * @param pV
     */
    public Perso(String nom, int PV) {
        this.nom = nom;
        this.MaxPV= PointsVie.PV;
        this.pv = this.MaxPV;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String s){
        this.nom = s;
    }

    public int getPV() {
        return this.pv;
    }

    public void setPV(int nb){
        this.pv = nb;
    }

    public int getMaxPV() {
        return this.MaxPV;
    }

    public void setMaxPV(int nb){
        if(nb>this.pv)
            this.MaxPV = nb;
        else
            this.MaxPV = this.pv;
    }

    public void obtenirNom(){
      System.out.print("Choisissez votre nom : ");
      (new DialoguePerso()).choisirNom();
    }

    public int obtenirPVDepart(){
      System.out.println("Choissisez votre niveau de difficulte : ");
      System.out.println("");
      System.out.println("   1) Facile : Profitez de la splendeur du massacre (100% PV)");
      System.out.println("   2) Moyen : J'ai envie d'une experience plus poussee (60% PV)");
      System.out.println("   3) Difficile : L'aventure avant tout (40% PV)");
      System.out.println("   4) Diabolique : NE FAITES PAS LA MOINDRE ERREUR !!! (20% PV)");
      System.out.println("");
      return (new DialoguePerso()).choisirDifficulte();
    }
}
