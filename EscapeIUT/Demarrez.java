package com.iut.escapeiut;
import java.util.Scanner;
import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Demarrez {

  public Demarrez(){}

  public void lancement(){
    String filePath = "./textes/demarrez/lancement.txt";
    System.out.println(Read.readLineByLine(filePath));
    Choix_lancement();
  }

  public void Choix_lancement(){
    System.out.println("CHOISISSEZ : Demmarrez(1) / Credits(2) / Objectif(3) / Copyright(4)");
    Scanner sc = new Scanner(System.in);
    int choix = sc.nextInt();
    switch (choix) {
      case 1:
        System.out.println("Lancement du jeu :");
        System.out.println("");
        break;
      case 2:
        String filePath2 = "./textes/demarrez/credits.txt";
        System.out.println(Read.readLineByLine(filePath2));
        Choix_lancement();
        break;
      case 3:
        String filePath3 = "./textes/demarrez/objectif.txt";
        System.out.println(Read.readLineByLine(filePath3));
        Choix_lancement();
        break;
      case 4:
        String filePath4 = "./textes/demarrez/copyright.txt";
        System.out.println(Read.readLineByLine(filePath4));
        Choix_lancement();
        break;
      default:
        System.out.println("Lancement du jeu :");
        System.out.println("");
        break;
      }
    }
  }
