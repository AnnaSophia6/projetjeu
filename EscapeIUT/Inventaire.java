import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Inventaire {

  public void Intro_Inventaire() {
    String filePath = "./textes/inventaire.txt";
    System.out.println(Read.readLineByLine(filePath));
    Affichage_objet_supplementaire();
  }

  public void Liste_inventaire(int e){

    if(e == 1){
      Inserer_rouleau_papier(5,true);
    }
    if (e == 2){
      Inserer_Vodka(true);
    }
    if (e == 3){
      Inserer_RGBD(true);
    }

    if (e == 4){
      Inserer_pont_papier(true);
    }

    if (e == 5){
      inserer_Cle_USB(true);
    }

    if (e == 6){
      Inserer_Justificatif(true);
    }

  }

  public void Affichage_objet_supplementaire(){
    String filePath2 = "./textes/inventaire_plus.txt";
    System.out.println(Read.readLineByLine(filePath2));
  }


  public void Inserer_rouleau_papier(int i, boolean b){
    if (b == true){
      System.out.println("Rouleau de papier toilette (X"+i+") : disponible dans les toilettes.");
      System.out.println("");
    }
  }

  public void Inserer_Vodka(boolean b){
    if (b == true){
      System.out.println("Bouteille Vodka : c’est FOU ce qu'on peut trouver en fouillant le bureau des profs.");
      System.out.println("");
    }
  }

    public void Inserer_pont_papier(boolean b){
      if (b == true){
        System.out.println("Pont en papier : pas facile a fabriquer, en realite");
        System.out.println("");
      }
    }

    public void Inserer_RGBD(boolean b){
      if (b == true){
        System.out.println("Livre de loi sur le RGPD : extremement ennuyant, a quoi pourrait-il servir dans le cadre d'une formation d'informatique ?");
        System.out.println("");
      }
    }

    public void inserer_Cle_USB(boolean b){
      if (b == true){
        System.out.println("Cle USB : contient le projet Sudoku.java");
        System.out.println("");
      }
    }

    public void Inserer_Justificatif(boolean b){
      if (b == true){
        System.out.println("Justificatif d'absence : ne depassez JAMAIS les 4 absences injustifiees, ou vous en subirez les consequences.");
        System.out.println("");
      }
    }






}
