package main;
import java.util.Scanner;
import java.util.Random;

public class FuiteFinale extends Fin 
{
    private int pasPerso, pasMonstre;
    private static int pas=0;
    private static int monstre=0;
    private static int compteurPas=0;
    private static int compteurMonstre=0;
    private static int total = compteurPas-compteurMonstre;

    public int avancer(){
        Random random = new Random(); 
        int nb = random. nextInt(6);
        return nb;
    }

    public int reculer(){
        Random random = new Random(); 
        int nb = random. nextInt(6);
        return nb;
    }

    public void commencerFuiteFinale(){
        dialogueFuiteFinale();
    }

    public void dialogueFuiteFinale(){
        System.out.println("Que faites-vous ?\n");
        System.out.println("   1)   Je cours.\n");
        System.out.println("   2)   Je lache un rouleau de papier toilette.\n");
        choisirCourirOuRouleau();
    }

    public void choisirCourirOuRouleau(){
        Scanner sc=new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            courir();
            break;
            case 2:
            lacherRouleau();
            break;
        }
    }

    public void dialogueBougerVous(){
        System.out.println("Vous avez bouge de " +pas+ " pas\n");
        System.out.println("Vous avez realise en tout " +compteurPas+ " pas\n");
    }

    public void dialogueBougerMonstre(){
        System.out.println("Votre adversaire a bouge de " +monstre+ " pas\n");
        System.out.println("Elle est a " +compteurMonstre+ "\n");
    }

    public void bouger(){
        bougerVous();
        bougerMonstre();
    }

    public void bougerVous(){
        pas = avancer();
        compteurPas+=pas;
        total = compteurPas - compteurMonstre;
        dialogueBougerVous();
    }

    public void bougerMonstre(){
        monstre = avancer();
        compteurMonstre+=monstre;
        total = compteurPas - compteurMonstre;
        dialogueBougerMonstre();
    }

    public void courir(){
        bouger();
        if(attraper()){
            declencherSequenceEchec();
        }
        else if(gagner()){
            System.out.println("Vous inserez la cle dans la serrure.\n");
            bougerMonstre();
            if(attraper())
                declencherSequenceEchec();
            else{
                System.out.println("Vous déverrouillez la porte.\n");
                bougerMonstre();
                if(attraper())
                    declencherSequenceEchec();
                else{
                    System.out.println("Vous poussez la porte de l’entrée principale et vous sortez de l’IUT, vous avez tellement peur que vous oubliez les escaliers et vous tombez. \n");
                    p.setPV(-2);
                    System.out.println("Vous subissez 2 point(s) de dégâts. Il vous reste " +p.getPV()+ " PV. \n");
                    if(p.getPV()==0)
                        (new Mort()).mourirTomber();
                    else
                        declencherSequenceReussite();
                }
            }
        }
        else{
            dialogueFuiteFinale();
        }
    }

    public void lacherRouleau(){
        pas = -(reculer());
        compteurMonstre+=pas;
    }

    public boolean attraper(){
        return (total==0);
    }

    public boolean gagner(){
        return (compteurPas>=50);
    }

    public void declencherSequenceEchec(){
        System.out.println("TENODAL vous a rattrapé ! Vous n'avez pas couru assez vite...\n");
        System.out.println("Vous allez subir la colere de TENODAL...\n");
        System.out.println("Plus rien ne peut l'arreter !\n");
        System.out.println("Faites vos prieres\n");
        System.out.println("HAHAHA Le voyage dans les iles M'APPARTIENT !\n");
        System.out.println("Et de 1...\n");
        System.out.println("Elle vous coupe les oreilles\n");
        System.out.println("Et de 2...\n");
        System.out.println("Elle vous perce les yeux\n");
        System.out.println("Et de 3...\n");
        System.out.println("Elle vous jette des cailloux\n");
        System.out.println("Et enfin...\n");
        System.out.println("Elle vous coupe la tete.\n");
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("                GAME OVER\n");
    }

    public void declencherSequenceReussite(){
        System.out.println("L’adversaire est vaincue \n");
        System.out.println("Vous vous relevez et vous courez le plus rapidement possible. Vous ne voyez absolument rien, le brouillard ne permet pas de voir à plus de 2 mètres.  \n");
        System.out.println("Vous courez, courez, courez… Le brouillard devient de plus en plus blanc et lumineux. \n");
        System.out.println("Vos yeux sont très douloureux, et c’est impossible de les fermer… \n");
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("DUT ! DUT ! DUT ! DUT ! DUT ! DUT ! DUT ! DUT ! DUT ! DUT ! \n");
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("Il est 6h30, on est le 13 mars 2020.  \n");
        System.out.println("VOUS : Mais quel cauchemar débile, il faut que j’arrête de jouer à Call of Ctulhu et à Resident Evil, ça m’a beaucoup trop monté à la tête.\n");
        System.out.println("Allez, c’est l’heure de se lever et d’aller affronter les créatures. \n");
        System.out.println("Euh, les profs.\n");
    }

}
