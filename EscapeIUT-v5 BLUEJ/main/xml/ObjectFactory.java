package main.xml;
import javax.xml.bind.annotation.XmlRegistry;

import com.iut.escapeiut.xml.Perso;

//JaxB en a besoin por appeller createPerso et remprir l'info de l'instance Perso avec l'info de xml

@XmlRegistry
public class ObjectFactory {
	
	public ObjectFactory() {
    }
	
	public Perso createPerso() {
        return new Perso();
    }	

}
