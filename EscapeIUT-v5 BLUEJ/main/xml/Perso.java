package main.xml;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.iut.escapeiut.DialoguePerso;



/**
 * @author asjos
 */


@XmlRootElement
@XmlType(propOrder= {"nom", "pv", "maxPV"})
public class Perso
{
    private String nom;
    private int pv;
    private int maxPV;

    
    /**
     * @param nom
     * @param pV
     */
    
    
    public Perso() {};
    public Perso(String nom, int pv) {
        this.nom = nom;
        this.maxPV= pv;
        this.pv = this.maxPV;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String s){
        this.nom = s;
    }

    public int getPv() {
        return this.pv;
    }

    public void setPv(int nb){
        this.pv = nb;
    }

    public int getMaxPV() {
        return this.maxPV;
    }

    public void setMaxPV(int nb){
        if(nb>this.pv)
            this.maxPV = nb;
        else
            this.maxPV = this.pv;
    }

    public void obtenirNom(){
      System.out.print("Choisissez votre nom : ");
      (new DialoguePerso()).choisirNom();
    }

    public int obtenirPVDepart(){
      String difficulte = "main/textes/demarrez/choisir_difficulte.txt";
      System.out.println(Read.readLineByLine(difficulte));
      return (new DialoguePerso()).choisirDifficulte();
    }
}
