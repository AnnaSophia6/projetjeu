package main;
import java.util.Scanner;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DialogueEgairam extends Dialogue {

  public void Intro_Egairam(){
    String intro = "main/textes/dialeg/dialeg_intro.txt";
    System.out.println(Read.readLineByLine(intro));
    /** recup choix a faire
    if( == "cafe")
    {
            ChoixCafe();
    }
    else if( == "nourriture")
    {
            ChoixNourriture();
    }
    else if( == "boisson")
    {
            ChoixBoisson();
    }
    else
    {
       String furax = "main/textes/dialeg/dial_notime.txt";
       System.out.println(Read.readLineByLine(furax));
       baisserPV();
    }
    **/
  }

  public int ChoixCafe()
  {
    String furax = "main/textes/dialeg/cafe_ask.txt";
    System.out.println(Read.readLineByLine(furax));
    Scanner sc = new Scanner(System.in);
    int choixfurax = sc.nextInt();

    if(choixfurax == 1 || choixfurax == 2 || choixfurax == 3)
    {
        String furax2 = "main/textes/dialeg/cafe_all.txt";
        System.out.println(Read.readLineByLine(furax2));
        baisserPV();
    }
    return p.getPV();
  }

  public int ChoixNourriture()
  {
    String furious = "main/textes/dialeg/cafe_ask.txt";
    System.out.println(Read.readLineByLine(furious));
    Scanner sc = new Scanner(System.in);
    int choixfurious = sc.nextInt();

    if(choixfurious == 1 || choixfurious == 2 || choixfurious == 3)
    {
        String furious2 = "main/textes/dialeg/nourri_all.txt";
        System.out.println(Read.readLineByLine(furious2));
        baisserPV();
    }
    return p.getPV();
  }

  public int ChoixBoisson()
  {
    String madness = "main/textes/dialeg/cafe_ask.txt";
    System.out.println(Read.readLineByLine(madness));
    Scanner sc = new Scanner(System.in);
    int choixmad = sc.nextInt();

    if(choixmad == 1 || choixmad == 2 || choixmad == 3)
    {
        String furious2 = "main/textes/dialeg/boisson_all.txt";
        System.out.println(Read.readLineByLine(furious2));
        baisserPV();
    }
    return p.getPV();
  }

  public void baisserPV()
  {
      p.setPV(-5);
      System.out.println("Vous avez pris 5 point(s) de degats, il vous reste " + p.getPV() + "/" + p.getMaxPV() + " PV.");
  }
}
