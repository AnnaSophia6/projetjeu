package main;
import java.util.Scanner;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DialogueToilettes extends Dialogue{

  public void Intro_Toilettes() {
     String intro = "main/textes/dial_wc/dial_wc_intro.txt";
     System.out.println(Read.readLineByLine(intro));
     choixCornelien();
  }

  public int choixCornelien()
  {
     String choixcornel = "main/textes/dial_wc/dial_wc_ask.txt";
     System.out.println(Read.readLineByLine(choixcornel));
     Scanner sc = new Scanner(System.in);
     int choix_wc = sc.nextInt();

     switch(choix_wc){
          case 1 :
          String filePath1 = "main/textes/dial_wc/dial_wc_rep1.txt";
          System.out.println(Read.readLineByLine(filePath1));
          break;
          case 2 :
          String filePath2 = "main/textes/dial_wc/dial_wc_rep2.txt";
          System.out.println(Read.readLineByLine(filePath2));
          break;
          case 3 :
          String filePath3 = "main/textes/dial_wc/dial_wc_rep3.txt";
          System.out.println(Read.readLineByLine(filePath3));
          break;
          case 4 :
          String filePath4 = "main/textes/dial_wc/dial_wc_rep4.txt";
          System.out.println(Read.readLineByLine(filePath4));
          break;
          case 5 :
          String filePath5 = "main/textes/dial_wc/dial_wc_rep5.txt";
          System.out.println(Read.readLineByLine(filePath5));
          break;
          case 6 :
          String filePath6 = "main/textes/dial_wc/dial_wc_rep6.txt";
          System.out.println(Read.readLineByLine(filePath6));
          break;
          default :
          System.out.println("Choix non reconnu");
          choixCornelien();
          break;
     }
     return p.getPV();
  }
}
