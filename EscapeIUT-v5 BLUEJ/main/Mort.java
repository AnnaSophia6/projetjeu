package main;

/**
 * @author asjos, ek
 */

public class Mort extends Game
{

  public static void GameOver()
  {
    String gameOver = "main/textes/game_over.txt";
    System.out.println(Read.readLineByLine(gameOver));
  }

  public void mourirTomber()
  {
      String dead = "main/textes/mort.txt";
      System.out.println(Read.readLineByLine(dead));
      GameOver();
  }
}
