package main;
import java.util.Scanner;
import java.util.Random;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Combat extends DialogueSecretariat {

  private int PV;
  private int PVmax;
  private int PVmonstre;
  private int PVmaxmonstre;
  private int degats;
  private int degatsmonstre;

  public Combat (int pv) {

    this.PV = pv;
    this.PVmax = 25;
    this.PVmaxmonstre = 15;
    this.PVmonstre = PVmaxmonstre;

  }

    public int attaquer(){
        Random random = new Random();
        int degats = random.nextInt(6);
        return degats;
    }

    public int subir(){
        Random random = new Random();
        int degats = random.nextInt(6);
        return degats;
    }

    public boolean TentativeAttaque() {
      Random random = new Random();
      int test = random.nextInt(2);
      if (test == 1){
        return true;
      }else{
        return false;
      }
    }

    public void commencerCombat(){

        dialogueCombat();
    }

    public void dialogueCombat(){
        String filePath = "main/textes/dialcombat.txt";
        System.out.println(Read.readLineByLine(filePath));
        choisirCombatOuRouleau();
    }

    public void choisirCombatOuRouleau(){
        Scanner sc=new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            Combat();
            break;
            case 2:
            lancerRouleau();
            break;
        }
    }

    public void dialogueCombatVous(int degats){

        System.out.println("Vous avez subit " +degats+ " point(s) de degats\n");
        System.out.println("");
        System.out.println("Vous avez  " +PV+ "/"+ PVmax+ "\n");
        System.out.println("");
    }

    public void dialogueCombatSecretaire(int degatsmonstre){

        System.out.println("Votre adversaire a subit " +degatsmonstre+ " point(s) de degats\n");
        System.out.println("");
        System.out.println("Elle est a " +PVmonstre+ "/"+ PVmaxmonstre+"\n");
        System.out.println("");
    }

    public void duel(){
        combatVous();
        combatMonstre();
    }

    public void combatVous(){
      System.out.println("Vous subissez une attaque");
      System.out.println("");
      if(TentativeAttaque()){
        System.out.println("Attaque adversaire reussie");
        System.out.println("");
        degats = subir();
        PV = PV - degats;
        dialogueCombatVous(degats);
    }else{
      System.out.println("Attaque echoue");
      System.out.println("");
      }
    }

    public void combatMonstre(){
      System.out.println("Vous realisez une attaque");
      System.out.println("");
      if(TentativeAttaque()){
        System.out.println("Attaque reussie");
        System.out.println("");
        degatsmonstre = attaquer();
        PVmonstre = PVmonstre - degatsmonstre;
        dialogueCombatSecretaire(degatsmonstre);
    }else{
      System.out.println("Attaque echoue");
      System.out.println("");
      }
    }


    public void Combat(){
        System.out.println("Combat face a face");
        System.out.println("");
        duel();
        if(PV<=0){
            declencherSequenceEchec();
        }
        else if(PVmonstre<=0){
            declencherSequenceReussite();
        }
        else
            dialogueCombat();
        }

    public void lancerRouleau(){
      System.out.println("Vous lancez un rouleau de papier");
      System.out.println("");
      combatMonstre();
      if(PV<=0){
          declencherSequenceEchec();
      }
      else if(PVmonstre<=0){
          declencherSequenceReussite();
      }
      else
          dialogueCombat();
      }

      public void declencherSequenceEchec(){
        System.out.println("Vous avez perdu");
        System.out.println("");
      }

      public void declencherSequenceReussite(){
        System.out.println("Vous avez gagne");
        System.out.println("");
      }

      /*public static void main(String[]args){
        Combat c = new Combat(22);
        c.commencerCombat();
      } */
    }
