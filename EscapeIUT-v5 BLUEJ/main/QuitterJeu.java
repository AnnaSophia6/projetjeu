package main;
import java.util.Scanner;

public class QuitterJeu {

  private String s;

  public QuitterJeu (String s){
    this.s = s;
  }


  public void quitter(){
    System.out.println("Voulez vous quittez notre jeu ? (tapez quitter) ");
    Scanner sc = new Scanner (System.in);
    String s = sc.nextLine();
    QuitterJeu quitter = new QuitterJeu(s);
    if(s.equals("quitter")){
      System.out.println("Merci d'avoir essaye notre jeu.  Au prochain massacre.");
    }else{
      System.out.println("Retour au choix precedent ");
    }
  }
}
