package main;
/**
 * Classe Game est un jeu en particulier avec un personnage
 * 
 * @author bernar276u, asjos
 *
 */
public class Game {
    public Perso p;
    
    public Game(){
        this.p = new Perso("inconnu");
    }
    
    public void startGame(){
        Demarrez de = new Demarrez();
        de.lancement();
        p.obtenirPVDepart();
        (new DialogueJeu()).intro();
    }

 }
