package main;
import java.util.Scanner;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DialogueSalle105 extends Dialogue {

   public void Intro_Salle105() {
      String intro = "main/textes/dial105/dial105_intro.txt";
      System.out.println(Read.readLineByLine(intro));
      choixPorte();
    }

   public int choixPorte(){
      String choix = "main/textes/dial105/dial105_ask.txt";
      System.out.println(Read.readLineByLine(choix));
      Scanner sc = new Scanner(System.in);
      int choixporte = sc.nextInt();

        switch(choixporte){
          case 1 :
          String filePath1 = "main/textes/dial105/dial105_ask_rep1.txt";
          System.out.println(Read.readLineByLine(filePath1));
          break;
          case 2 :
          String filePath2 = "main/textes/dial105/dial105_ask_rep2.txt";
          System.out.println(Read.readLineByLine(filePath2));
          break;
          case 3 :
          String filePath3 = "main/textes/dial105/dial105_ask_rep3.txt";
          System.out.println(Read.readLineByLine(filePath3));
          break;
          case 4 :
          String filePath4 = "main/textes/dial105/dial105_ask_rep4.txt";
          System.out.println(Read.readLineByLine(filePath4));
          break;
          case 5 :
          String filePath5 = "main/textes/dial105/dial105_ask_rep5.txt";
          System.out.println(Read.readLineByLine(filePath5));
          break;
          case 6 :
          String filePath6 = "main/textes/dial105/dial105_ask_rep6.txt";
          System.out.println(Read.readLineByLine(filePath6));
          break;
          case 7 :
          String filePath7 = "main/textes/dial105/dial105_ask_rep7.txt";
          System.out.println(Read.readLineByLine(filePath7));
          baisserPV();
              if (p.getPV() <= 0)
              {
                  Mort.GameOver();
              }
          String filePath8 = "main/textes/dial105/dial105_ask_rep7_2.txt";
          System.out.println(Read.readLineByLine(filePath8));
          break;
          default :
          System.out.println("Choix non reconnu");
          choixPorte();
          break;
        }
      return p.getPV();
   }

   public int choixPhilo(){
      String ask2 = "main/textes/dial105/dial105_ask2_rep7.txt";
      System.out.println(Read.readLineByLine(ask2));
      Scanner sc = new Scanner(System.in);
      int choixphilo = sc.nextInt();

      switch(choixphilo){
          case 1 :
          String filePath1 = "main/textes/dial105/dial105_ask2_rep1.txt";
          System.out.println(Read.readLineByLine(filePath1));
          baisserPV();
              if (p.getPV() <= 0)
              {
                  Mort.GameOver();
              }
          break;
          case 2 :
          String filePath2 = "main/textes/dial105/dial105_ask2_rep2.txt";
          System.out.println(Read.readLineByLine(filePath2));
          break;
          case 3 :
          String filePath3 = "main/textes/dial105/dial105_ask2_rep3.txt";
          System.out.println(Read.readLineByLine(filePath3));
          break;
          case 4 :
          String filePath4 = "main/textes/dial105/dial105_ask2_rep4.txt";
          System.out.println(Read.readLineByLine(filePath4));
          choixPapier();
          break;
          default :
          System.out.println("Choix non reconnu");
          choixPhilo();
          break;
        }
      return p.getPV();
    }

    public int choixPapier(){
       String filePath = "main/textes/dial105/dial105_ask3.txt";
       System.out.println(Read.readLineByLine(filePath));
       Scanner sc = new Scanner(System.in);
       int choixpapier = sc.nextInt();

      switch(choixpapier){
          case 1 :
          String filePath1 = "main/textes/dial105/dial105_ask3_rep1.txt";
          System.out.println(Read.readLineByLine(filePath1));

          break;
          case 2 :
          String filePath2 = "main/textes/dial105/dial105_ask3_rep2.txt";
          System.out.println(Read.readLineByLine(filePath2));
          break;
          case 3 :
          String filePath3 = "main/textes/dial105/dial105_ask3_rep3.txt";
          System.out.println(Read.readLineByLine(filePath3));
          break;
          case 4 :
          String filePath4 = "main/textes/dial105/dial105_ask3_rep4.txt";
          System.out.println(Read.readLineByLine(filePath4));
          break;
          case 5 :
          String filePath5 = "main/textes/dial105/dial105_ask3_rep5.txt";
          System.out.println(Read.readLineByLine(filePath5));
          break;
          default :
          System.out.println("Choix non reconnu");
          choixPapier();
          break;
        }
      return p.getPV();
    }

    public void baisserPV(){
        p.setPV(-5);
        System.out.println("Vous avez pris 5 point(s) de degats, il vous reste " + p.getPV() + "/" + p.getMaxPV() + " PV.");
    }
}
