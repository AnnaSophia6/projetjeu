package main;
import java.util.Scanner;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DialogueJeu extends Dialogue {

    public void intro() {
        String dialogue_jeu = "main/textes/intro_dial_jeu.txt";
        System.out.println(Read.readLineByLine(dialogue_jeu));
        (new DialogueTenib()).AppelTenib();
    }

    public void donnerPV() {
        String choix_diff = "main/textes/choix_difficulte.txt";
        System.out.println(Read.readLineByLine(choix_diff));
        (new DialoguePerso()).choisirDifficulte();
    }

    public void PremierCouloir() {
        String choix_salle = "main/textes/premier_couloir.txt";
        System.out.println(Read.readLineByLine(choix_salle));       
        //(new DialoguePerso()).choisirSalle();
    }

  
    // public void Salle_Informatique() {
    // }

    // public void Bureau_Prof() {
    // }

    // public void Salle105() {
    // }

    // public void Toilettes() {
    // }

    // public void Inventaire() {
    // }
    
    public void introFuiteFinale(){
        String fuite = "main/textes/intro_fuite.txt";
        System.out.println(Read.readLineByLine(fuite));
        
        System.out.println("Vous pouvez faire jusqu'a  6 points de degats et vous possedez actuellement " +p.getPV()+ "\n");
        
        String fuite2 = "main/textes/intro_fuite.txt";
        System.out.println(Read.readLineByLine(fuite2));
        (new DialoguePerso()).choisirFuiteFinale();
    }
    
    public void fautVraimentEtreBete(){
        System.out.println("Hein ? Il faut vraiment etre bete pour choisir cette option...");
    }
}