package main;
/**
 * @author asjos
 */

public class Perso
{
    private String nom;
    private int pv;
    private int MaxPV;

    /**
     * @param nom
     * @param pV
     */
    public Perso(String nom) {
        this.nom = nom;
        this.pv = PointsVie.PV;
        this.MaxPV = this.pv;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String s){
        this.nom = s;
    }

    public int getPV() {
        return this.pv;
    }

    public void setPV(int nb){
        this.pv = nb;
    }

    public int getMaxPV() {
        return this.MaxPV;
    }

    public void setMaxPV(int nb){
        if(nb>this.pv)
            this.MaxPV = nb;
        else
            this.MaxPV = this.pv;
    }

    public void obtenirPVDepart(){
        (new DialogueJeu()).donnerPV();
    }
}