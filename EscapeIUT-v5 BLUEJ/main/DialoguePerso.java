package main;
import java.util.Scanner;

public class DialoguePerso extends Dialogue{ 

    public void choisirDifficulte(){
        Scanner sc=new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            p.setPV(PointsVie.FACILE);
            break;
            case 2:
            p.setPV(PointsVie.MOY);
            break;
            case 3:
            p.setPV(PointsVie.DIFF);
            break;
            case 4:
            p.setPV(PointsVie.DIFF);
            break;
            default:
            System.out.println("erreur. L'univers t'accorde " +PointsVie.MOY+ " pts.");
            p.setPV(PointsVie.MOY);
            p.setMaxPV(PointsVie.MOY);
            break;
        }
    }
    
    // public static void choisirSalle(){
        // int choix = sc.nextInt();
        // switch (choix) {
            // case 1:
            // dj.Salle_Informatique();
            // break;
            // case 2:
            // dj.Bureau_Prof();
            // break;
            // case 3:
            // dj.Salle105();
            // break;
            // case 4:
            // dj.Toilettes();
            // break;
            // case 5:
            // dj.Inventaire();
            // break;
            // case 6:
            // (new Plan(1,1)).afficherPlan();
            // dj.PremierCouloir();
            // break;
            // default:
            // System.out.println("Choix non reconnu");
            // dj.PremierCouloir();
            // break;
        // }
    // }
    
    public void choisirFuiteFinale(){
        Scanner sc=new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            (new DialogueJeu()).fautVraimentEtreBete();
            (new DialogueJeu()).introFuiteFinale();
            break;
            case 2:
            System.out.println("La fuite peut commencer.");
            break;
        }
    }
}