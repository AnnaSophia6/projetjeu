package main;
import java.util.Scanner;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DialogueTenib extends Dialogue {

    public void AppelTenib() {
        Scanner sc = new Scanner(System.in);
        String filePath = "main/textes/dial_tenib/appeltenib.txt";
        System.out.println(Read.readLineByLine(filePath));
        Choix_AppelTenib();

    }

    public int Choix_AppelTenib() {
        String filePath2 = "main/textes/dial_tenib/appeltenib2.txt";
        System.out.println(Read.readLineByLine(filePath2));
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            String filePath3 = "main/textes/dial_tenib/tenibappel_choix1.txt";
            System.out.println(Read.readLineByLine(filePath3));
            ReciteRGPD();
            break;
            case 2:
            String filePath4 = "main/textes/dial_tenib/tenibappel_choix2.txt";
            System.out.println(Read.readLineByLine(filePath4));
            ReciteRGPD();
            break;
            case 3:
            String filePath5 = "main/textes/dial_tenib/tenibappel_choix3.txt";
            System.out.println(Read.readLineByLine(filePath5));
            ReciteRGPD();
            break;
            case 4:
            String filePath6 = "main/textes/dial_tenib/tenibappel_choix4.txt";
            System.out.println(Read.readLineByLine(filePath6));
            baisserPV();
            if (p.getPV() <= 0) {
                Mort.GameOver();
                break;
            } else {
                ReciteRGPD();
                break;
            }
            default:
            System.out.println("Choix non reconnu");
            Choix_AppelTenib();
            break;
        }
        return p.getPV();
    }

    public void ReciteRGPD() {
        String filePath7 = "main/textes/dial_tenib/reciteRGPD.txt";
        System.out.println(Read.readLineByLine(filePath7));
        ProfTableau();
    }

     public void ProfTableau() {
        String filePath8 = "main/textes/dial_tenib/proftableau.txt";
        System.out.println(Read.readLineByLine(filePath8));
        Choix_ProfTableau();
    }

    public int Choix_ProfTableau() {
        String filePath9 = "main/textes/dial_tenib/proftableau_suite.txt";
        System.out.println(Read.readLineByLine(filePath9));
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            System.out.println("Vous attendez");
            for (int i = 0; i < 100; i++) {
                System.out.println("...");
            }
            System.out.println("C'est long n'est-ce pas ?");
            for (int i = 0; i < 100; i++) {
                System.out.println("...");
            }
            System.out.println("Il serait plus interessant de faire autre chose");
            Choix_ProfTableau();
            break;
            case 2:
            String filePath10 = "main/textes/dial_tenib/proftableau_case2.txt";
            System.out.println(Read.readLineByLine(filePath10));
            Choix_ProfTableau();
            break;
            case 3:
            String filePath11 = "main/textes/dial_tenib/proftableau_case3.txt";
            System.out.println(Read.readLineByLine(filePath11));
            TentativeFuite_TENIB();
            break;
            case 4:
            String filePath12 = "main/textes/dial_tenib/proftableau_case4.txt";
            System.out.println(Read.readLineByLine(filePath12));
            baisserPV();
            if (p.getPV() <= 0) {
                Mort.GameOver();
                break;
            } else {
                Choix_ProfTableau();
                break;
            }
            default:
            System.out.println("Choix non reconnu");
            Choix_ProfTableau();
            break;
        }
        return p.getPV();
    }

    public int TentativeFuite_TENIB() {
        String TentativeFuite = "main/textes/dial_tenib/fuite_tenib.txt";
        System.out.println(Read.readLineByLine(TentativeFuite));
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            String TentativeFuite1 = "main/textes/dial_tenib/fuite_tenib_case1.txt";
            System.out.println(Read.readLineByLine(TentativeFuite1));
            baisserPV();
            if (p.getPV() <= 0) {
                Mort.GameOver();
                break;
            } else {
                Choix_ProfTableau();
                break;
            }
            case 2:
            String TentativeFuite2 = "main/textes/dial_tenib/fuite_tenib_case1.txt";
            System.out.println(Read.readLineByLine(TentativeFuite2));
            baisserPV();
            if (p.getPV() <= 0) {
                Mort.GameOver();
                break;
            } else {
                Choix_ProfTableau();
                break;
            }
            case 3:
            String TentativeFuite3 = "main/textes/dial_tenib/fuite_tenib_case3.txt";
            System.out.println(Read.readLineByLine(TentativeFuite3));
            Choix_ProfTableau();
            break;
            case 4:
            String TentativeFuite4 = "main/textes/dial_tenib/fuite_tenib_case4.txt";
            System.out.println(Read.readLineByLine(TentativeFuite4));
            (new DialogueJeu()).PremierCouloir();
            break;
            default:
            System.out.println("Choix non reconnu");
            TentativeFuite_TENIB();
            break;
        }
        return p.getPV();
    }


    public void baisserPV(){
        p.setPV(-5);
        System.out.println("Vous avez pris 5 point(s) de degats, il vous reste " + p.getPV() + "/" + p.getMaxPV() + " PV.");
    }

}
